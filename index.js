console.log("hi")
// console.log("document");

const txtFirstName = document.querySelector("#txt-first-name");
console.log(txtFirstName);
const txtLastName = document.querySelector("#txt-last-name")
//result: input field / tag
/*
	alternative wayys:
		>> document.getElementById("txt-first-name");
		>> document.getElementByClassName("text-class");
		>> document.getElementByTagName("h1")

*/

const spanFullName = document.querySelector("#span-full-name");
console.log(spanFullName);

// Event Listeners
// event can have shorthand of (e)

// txtFirstName.addEventListener('keyup',(event) => {

// 	spanFullName.innerHTML = txtFirstName.value + " ";

// });


// const keyCodeEvent = (e) => {
// 	let kc = e.keyCode;
// 	if (kc === 65){
// 		e.target.value = null;
// 		alert('Someone clicked a')
// 	}
// }

// txtFirstName.addEventListener('keyup', keyCodeEvent);

// A C T I V I T Y

const firstNameEvent = (a) => {
	spanFullName.innerHTML = a.target.value + " " + txtLastName.value;
}

const lastNameEvent = (a) => {
	spanFullName.innerHTML =  txtFirstName.value + " " + a.target.value;
}

txtFirstName.addEventListener('keyup', firstNameEvent);
txtLastName.addEventListener('keyup', lastNameEvent);